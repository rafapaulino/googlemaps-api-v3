<?php include 'header.html'; ?>

  <div class="row">
    <div class="col-md-12">
      <p><strong>Ao clicar nos botões abaixo os valores do mapa são pegos.</strong></p>
      <button id="getValues" class="btn btn-primary">Pegar valores</button>
      <button id="changeValues" class="btn btn-success">Alterar valores</button>
    </div>
    <div class="col-md-12">
      <div id="map" class="valores"></div>
    </div>
  </div>

<script type="text/javascript" src="assets/js/valores.js"></script>
<?php include 'footer.html'; ?>