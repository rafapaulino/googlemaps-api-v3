<?php include 'header.html'; ?>

  <div class="row">
    <div class="col-md-12">
		<p>Adiciona marcadores para cidades e estados, clique no botão abaixo para ver os marcadores das cidades.</p>
		<button class="btn btn-primary" id="estados">Ver estados</button>
		<button class="btn btn-danger" id="cidades">Ver cidades</button>
		<div id="map" class="valores"></div>
    </div>
  </div>

<script type="text/javascript" src="assets/js/lib/markermanager_packed.js"></script>
<script type="text/javascript" src="assets/js/cidades-estados.js"></script>
<?php include 'footer.html'; ?>