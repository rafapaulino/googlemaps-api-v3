<?php include 'header.html'; ?>

	<div class="row">
		<div class="col-md-12">
			<p>Exibe a rota para chegar ao local:</p><br>
			<form id="addressForm" action="">
				<div class="form-group col-md-4 col-md-offset-1">
					<label for="address">Coloque o endereço para onde você deseja ir:</label>
					<input type="text" name="address" id="address" class="form-control" placeholder="Coloque o endereço do lugar onde você deseja ir...">					
				</div>

				<div class="form-group col-md-4 col-md-offset-2">
					<label for="go">Coloque como você deseja ir:</label>
					<select class="form-control willgo" id="go">
	                    <option value="DRIVING">Digirindo</option>
	                    <option value="WALKING">Caminhando</option>
	                    <option value="BICYCLING">De bicicleta</option>
	                    <option value="TRANSIT">De transporte público</option>
	                </select>
	            </div>
			</form>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<div id="map" class="rotas"></div>
		</div>
		<div class="col-md-6">
			<div id="rota" class="rotas"></div>
		</div>
	</div>

<script type="text/javascript" src="assets/js/lib/geo.js"></script>
<script type="text/javascript" src="assets/js/rotas.js"></script>
<?php include 'footer.html'; ?>