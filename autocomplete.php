<?php include 'header.html'; ?>

  <div class="row">
    <div class="col-md-12">
		<p>Vai para o endereço informado no campo abaixo:</p>
		<form id="addressForm" action="">
			<div class="form-group col-md-8 col-md-offset-2">
				<label for="address">Endereço:</label>
				<input type="text" name="address" id="address" class="form-control" placeholder="Coloque o endereço aqui...">					
			</div>
		</form>
		<div id="map" class="valores"></div>
    </div>
  </div>

<script type="text/javascript" src="assets/js/autocomplete.js"></script>
<?php include 'footer.html'; ?>