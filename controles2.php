<?php include 'header.html'; ?>

  <div class="row">
    <div class="col-md-6">
      <div id="map1" class="mapsControles"></div>
      <p><strong>Controles deabailitados</strong></p>
    </div>
    <div class="col-md-6">
      <div id="map2" class="mapsControles"></div>
      <p><strong>Controle tipo Small</strong></p>
    </div>
  </div>

  <div class="row">
    <div class="col-md-6">
      <div id="map3" class="mapsControles"></div>
      <p><strong>Controle tipo Android</strong></p>
    </div>
    <div class="col-md-6">
      <div id="map4" class="mapsControles"></div>
      <p><strong>Controle tipo zoom pan</strong></p>
    </div>
  </div>

  <div class="row">
    <div class="col-md-6">
      <div id="map5" class="mapsControles"></div>
      <p><strong>Controle de Escala desabilitado</strong></p>
    </div>
    <div class="col-md-6">
      <div id="map6" class="mapsControles"></div>
      <p><strong>Navegação pelo teclado desabilitada</strong></p>
    </div>
  </div>

  <div class="row">
    <div class="col-md-6">
      <div id="map7" class="mapsControles"></div>
      <p><strong>Zoom desabilitado no duplo clique</strong></p>
    </div>
    <div class="col-md-6">
      <div id="map8" class="mapsControles"></div>
      <p><strong>Mover o mapa desabilitado</strong></p>
    </div>
  </div>
  
  <div class="row">
    <div class="col-md-6">
      <div id="map9" class="mapsControles"></div>
      <p><strong>Rolagem desabilitada</strong></p>
    </div>
    <div class="col-md-6">
      <div id="map10" class="mapsControles"></div>
      <p><strong>StreetView desabilitado</strong></p>
    </div>
  </div>

<script type="text/javascript" src="assets/js/controles2.js"></script>
<?php include 'footer.html'; ?>