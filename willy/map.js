﻿(function() {
    window.onload = function() {
        
        //latitude e longitude de sao paulo 
        const mapCenter = new google.maps.LatLng(-23.56289973679937, -46.62726103408666);

        //div com o mapa
        const mapDiv = document.getElementById('map');

        //configuracoes iniciais do mapa
        let mapConfig = {
            center: mapCenter,
            zoom: 12,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false
        }

        //cria o mapa
        let map = new google.maps.Map(mapDiv, mapConfig);

        //cria o objeto que serve como delimitador da area de visao do mapa
        let bounds = new google.maps.LatLngBounds();

        //lista de imoveis
        const places = [
            {
                name: 'Ibiatã Setin / Cód: 107261',
                address: 'Rua Sampaio Viana, 671, Paraíso - São Paulo - SP',
                image: 'https://cdn.vistahost.com.br/npinegoc/vista.imobi/fotos/16326/itZQbkR61q3yu29k9e_1632666fc4edef3ef7.jpg',
                price: 'R$ 4.972.000,00',
                url: 'https://www.npiconsultoria.com.br/imovel-107261/ibiata-setin',
                langLng: new google.maps.LatLng(-23.57527191173597, -46.649985221005345)
            },
            {
                name: 'Porto Paraiso Amy / Cód: 107096',
                address: 'Rua Coronel Oscar Porto, 609, Paraíso - São Paulo - SP',
                image: 'https://cdn-cdnhi.nitrocdn.com/pZAdOjjHnVeoiufBAzRuxLZqhrnivvYL/assets/images/optimized/rev-07e75da/cdn.vistahost.com.br/npinegoc/vista.imobi/fotos/16161/is3h53L37L3eNF_1616166df28b807b9f.jpg',
                price: '',
                url: 'https://www.npiconsultoria.com.br/imovel-107096/porto-paraiso-amyn',
                langLng: new google.maps.LatLng(-23.57419367619732, -46.64635160566174)
            },
            {
                name: 'Condomínio Vista Ibirapuera Cobertura / Cód: 107143',
                address: 'Rua Maria Figueiredo, 633, Paraíso - São Paulo - SP',
                image: 'https://cdn.vistahost.com.br/npinegoc/vista.imobi/fotos/16326/itZQbkR61q3yu29k9e_1632666fc4edef3ef7.jpg',
                price: 'R$ 15.000.000,00',
                url: 'https://www.npiconsultoria.com.br/imovel-107143/condominio-vista-ibirapuera-cobertura',
                langLng: new google.maps.LatLng(-23.57333954282853, -46.65113276333366)
            },
            {
                name: 'Mário Amaral Lindenberg',
                address: 'Rua Mário Amaral, 310, Paraíso - São Paulo - SP',
                image: 'https://cdn-cdnhi.nitrocdn.com/pZAdOjjHnVeoiufBAzRuxLZqhrnivvYL/assets/images/optimized/rev-07e75da/cdn.vistahost.com.br/npinegoc/vista.imobi/fotos/16168/ip6N93Hx4f_1616866e077d927a9d.jpg',
                price: '',
                url: 'https://www.npiconsultoria.com.br/imovel-107103/mario-amaral-lindenberg',
                langLng: new google.maps.LatLng(-23.572989141318068, -46.64893776333364)
            },
            {
                name: 'Maison Victória Rua Curitiba / Cód: 106977',
                address: 'Rua Curitiba, 31, Paraíso - São Paulo - SP',
                image: 'https://cdn-cdnhi.nitrocdn.com/pZAdOjjHnVeoiufBAzRuxLZqhrnivvYL/assets/images/optimized/rev-07e75da/cdn.vistahost.com.br/npinegoc/vista.imobi/fotos/16042/isUG7hVm7TrP62_1604266ca686cf3bf4.jpg',
                price: 'R$ 28.950.000,00',
                url: 'https://www.npiconsultoria.com.br/imovel-106977/maison-victoria-rua-curitiba',
                langLng: new google.maps.LatLng(-23.579356352700138, -46.65401430566149)
            },
            {
                name: 'Condomínio Edifício Ana Luiza / Cód: 106968',
                address: 'Rua Doutor Tomás Carvalhal, 880, Paraíso - São Paulo - SP',
                image: 'https://cdn-cdnhi.nitrocdn.com/pZAdOjjHnVeoiufBAzRuxLZqhrnivvYL/assets/images/optimized/rev-07e75da/cdn.vistahost.com.br/npinegoc/vista.imobi/fotos/16030/i31eCNm78YuA4935lWN_1603066c7cc456f478.jpg',
                price: 'R$ 3.250.000,00',
                url: 'https://www.npiconsultoria.com.br/imovel-106968/condominio-edificio-ana-luiza',
                langLng: new google.maps.LatLng(-23.578315883205523, -46.649403249841015)
            },
            {
                name: 'Condomínio Edifício Up Side / Cód: 106509',
                address: 'Rua Doutor Eduardo Amaro, 99, Paraíso - São Paulo - SP',
                image: 'https://cdn-cdnhi.nitrocdn.com/pZAdOjjHnVeoiufBAzRuxLZqhrnivvYL/assets/images/optimized/rev-07e75da/cdn.vistahost.com.br/npinegoc/vista.imobi/fotos/15570/i858qR17uS1uBA4_15570660c6b9ac5dcc.jpg',
                price: '',
                url: 'https://www.npiconsultoria.com.br/imovel-106509/condominio-edificio-up-side',
                langLng: new google.maps.LatLng(-23.574848577634462, -46.63960920566168)
            },
            {
                name: 'Condomínio Ed Amir / Cód: 106392',
                address: 'Rua Abílio Soares, 122, Paraíso - São Paulo - SP',
                image: 'https://cdn-cdnhi.nitrocdn.com/pZAdOjjHnVeoiufBAzRuxLZqhrnivvYL/assets/images/optimized/rev-07e75da/cdn.vistahost.com.br/npinegoc/vista.imobi/fotos/15453/iE1F7KrpK3_1545365b91e175decd.jpg',
                price: 'R$ 1.180.000,00',
                url: 'https://www.npiconsultoria.com.br/imovel-106392/condominio-ed-amir',
                langLng: new google.maps.LatLng(-23.57316897511409, -46.64295126333364)
            },
            {
                name: 'Biotique Ibirapuera / Cód: 106123',
                address: 'Rua Manuel da Nobrega, 778, Paraíso - São Paulo - SP',
                image: 'https://cdn.vistahost.com.br/npinegoc/vista.imobi/fotos/15183/iH74UDG51o4_15183645d77f2b8af4.jpg',
                price: 'R$ 6.120.000,00',
                url: 'https://www.npiconsultoria.com.br/imovel-106123/biotique-ibirapuera',
                langLng: new google.maps.LatLng(-23.573227742199546, -46.653278334497685)
            },
            {
                name: 'Artesano Oscar Porto / Cód: 106014',
                address: 'Rua Coronel Oscar Porto, 507, Paraíso - São Paulo - SP',
                image: 'https://cdn.vistahost.com.br/npinegoc/vista.imobi/fotos/15071/i26G7J9mMw7451499dW2_150716332265dbbbad.jpg',
                price: 'R$ 4.638.000,00',
                url: 'https://www.npiconsultoria.com.br/imovel-106014/artesano-oscar-porto',
                langLng: new google.maps.LatLng(-23.573320240947723, -46.64678059216941)
            }
        ];

        // Função para criar um marcador e associar um InfoWindow
        function createMarker(place) {
            var marker = new markerWithLabel.MarkerWithLabel({
                icon: 'icone.png',
                position: place.langLng,
                clickable: true,
                draggable: false,
                map: map,
                labelContent: place.price,
                labelAnchor: new google.maps.Point(-75, 1),
                labelClass: "labels",
                labelStyle: { opacity: 1.0 },
            });

            var content = '<div class="info-window">' +
                '<h2>' + place.name + '</h2>' +
                '<img src="' + place.image + '" alt="' + place.name + '" class="image" />' + 
                '<h3><strong>Endereço:</strong> ' + place.address + '</h3>' +
                '<h4><strong>Preço:</strong> ' + place.price + '</h4>' +
                '<p><a href="' + place.url + '" target="_blank">Ver detalhes do imóvel</a></p>' +
            '</div>';

            var iw = new google.maps.InfoWindow({
                content: content,
            });

            google.maps.event.addListener(marker, "click", function() {
                iw.open(map, marker);
            });

            bounds.extend(place.langLng);
        }

        for (var i = 0; i < places.length; i++) { 
            createMarker(places[i]);
        }


        //ajusta o zoom do mapa de acordo com a area de pins
        map.fitBounds(bounds);

    }
})();