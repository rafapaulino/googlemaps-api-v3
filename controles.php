<?php include 'header.html'; ?>

  <div class="row">
    <div class="col-md-6">
      <div id="map1" class="mapsControles"></div>
      <p><strong>Controle Padrão</strong></p>
    </div>
    <div class="col-md-6">
      <div id="map2" class="mapsControles"></div>
      <p><strong>Mapa com a interface de usuario desabilitada</strong></p>
    </div>
  </div>

  <div class="row">
    <div class="col-md-6">
      <div id="map3" class="mapsControles"></div>
      <p><strong>Controle do mapa com a mapTypeControl desabilitada (mudança do tipo de mapa)</strong></p>
    </div>
    <div class="col-md-6">
      <div id="map4" class="mapsControles"></div>
      <p><strong>Controle do mapa com a mapTypeControl exibindo um menu drop-down</strong></p>
    </div>
  </div>

  <div class="row">
    <div class="col-md-6">
      <div id="map5" class="mapsControles"></div>
      <p><strong>Controle do mapa com a mapTypeControl exibindo a opção de barra horizontal e alinhamento à direita</strong></p>
    </div>
    <div class="col-md-6">
      <div id="map6" class="mapsControles"></div>
      <p><strong>Controle do mapa com a mapTypeControl exibindo a opção default e com a barra alinhada no rodapé</strong></p>
    </div>
  </div>

  <div class="row">
    <div class="col-md-6">
      <div id="map7" class="mapsControles"></div>
      <p><strong>Controle do mapa com a mapTypeControl exibindo a opção default e com a barra alinhada à esquerda</strong></p>
    </div>
    <div class="col-md-6">
      <div id="map8" class="mapsControles"></div>
      <p><strong>Controle do mapa com a mapTypeControl exibindo a opção default e com a barra alinhada no topo</strong></p>
    </div>
  </div>
  
  <div class="row">
    <div class="col-md-6">
      <div id="map9" class="mapsControles"></div>
      <p><strong>Controle do mapa com a mapTypeControl exibindo a opção default e com a barra alinhada no topo à esquerda</strong></p>
    </div>
    <div class="col-md-6">
      <div id="map10" class="mapsControles"></div>
      <p><strong>Mapa com todas as opções de controles de tipo de mapa</strong></p>
    </div>
  </div>

  <div class="row">
    <div class="col-md-6">
      <div id="map11" class="mapsControles"></div>
      <p><strong>Mapa com 1 das opções de controles de tipo de mapa</strong></p>
    </div>
    <div class="col-md-6">
      <div id="map12" class="mapsControles"></div>
      <p><strong>Mapa com 1 das opções de controles de tipo de mapa</strong></p>
    </div>
  </div>

<script type="text/javascript" src="assets/js/controles.js"></script>
<?php include 'footer.html'; ?>