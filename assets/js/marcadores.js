(function() {
 window.onload = function() {
   
   
   //http://code.google.com/intl/pt-BR/apis/maps/documentation/javascript/overlays.html
   //criando as propriedades do mapa 
   var options1 = {
    zoom: 3,
    center: new google.maps.LatLng(40.7257, -74.0047),
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    noClear: true ,
    backgroundColor: '#FFFFFF',
    draggableCursor: 'move',
    draggingCursor: 'move',
    scrollwheel: false
  };
   
  var map1 = new google.maps.Map(document.getElementById('map'), options1);

  //adicionando o marcador
  var marker1 = new google.maps.Marker({
    position: new google.maps.LatLng(40.7257, -74.0047),
    map: map1,
    title: 'Clique aqui!',
    icon: 'assets/images/aviao.png'
  });

  //adicionando o marcador
  var marker2 = new google.maps.Marker({
    position: new google.maps.LatLng(40.6257, -83.0047),
    map: map1,
    title: 'Alguem me ajude por favor!',
    icon: 'http://gmaps-samples.googlecode.com/svn/trunk/markers/green/marker1.png'
  });
   
   
  //adicionando o marcador
  var marker3 = new google.maps.Marker({
    position: new google.maps.LatLng(38.6257, -90.6047),
    map: map1,
    title: 'Olha este aqui!'      
  });

  // Creating an InfoWindow with the content text: "Hello World"
  var infowindow = new google.maps.InfoWindow({
    content: 'Oi, tudo bom?'
  });
 
  // Adding a click event to the marker
  google.maps.event.addListener(marker1, 'click', function() {
    // Calling the open method of the infoWindow
    infowindow.open(map1, marker1);
  });
};
})();