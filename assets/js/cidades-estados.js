(function() {

  window.onload = function() {

    var latLng = new google.maps.LatLng(37.99, -93.77);
    var options = {
      zoom: 3,
      center: latLng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById('map'), options);

    //MarkerManager  
    var mgr = new MarkerManager(map);

    // Criando o array com os estados do Colorado e Utah
    var states = [
      new google.maps.Marker({
        position: new google.maps.LatLng(39.4698, -111.5962),
        icon: 'assets/images/cluster.png'
      }),
      new google.maps.Marker({
        position: new google.maps.LatLng(38.9933, -105.6196),
        icon: 'assets/images/cluster.png'
      })
    ];

    // Criando o array para as cidades
    var cities = [
      // Colorado Springs
      new google.maps.Marker({position: new google.maps.LatLng(38.8338, -104.8213)}), 
      // Denver
      new google.maps.Marker({position: new google.maps.LatLng(39.7391, -104.9847)}),
      // Glenwood Springs
      new google.maps.Marker({position: new google.maps.LatLng(39.5505, -107.3247)}), 
      // Salt Lake City
      new google.maps.Marker({position: new google.maps.LatLng(40.7607, -111.8910)}), 
      // Fillmore
      new google.maps.Marker({position: new google.maps.LatLng(38.9688, -112.3235)}), 
      // Spanish Fork
      new google.maps.Marker({position: new google.maps.LatLng(40.1149, -111.6549)})
    ];

    //adiciona os marcadores após o mapa ser carregado
    google.maps.event.addListener(mgr, 'loaded', function() {

      //estados visiveis após o zoom 5
      mgr.addMarkers(states, 1, 5);

      //cidades vísiveis após o zoom 6
      mgr.addMarkers(cities, 6);

      //adiciona os marcadores
      mgr.refresh();

    });

    //ver as cidades
    document.getElementById('cidades').onclick = function() {
        map.setZoom(6);
        map.setCenter(latLng);
    };

    //ver as estados
    document.getElementById('estados').onclick = function() {
        map.setZoom(3);
    };

  };

})();