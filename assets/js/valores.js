(function() {
window.onload = function() {

   //criando as propriedades do mapa 
   var options1 = {
        zoom: 3,
        center: new google.maps.LatLng(17.55, 45.13),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        noClear: true ,
        backgroundColor: '#FFFFFF',
        draggableCursor: 'move',
        draggingCursor: 'move',
        scrollwheel: false
     };
   
   var map1 = new google.maps.Map(document.getElementById('map'), options1);
   
  // pegando os valores
  document.getElementById('getValues').onclick = function() {
    alert('O zoom atual é: ' + map1.getZoom());
    alert('O centro atual do mapa é: ' + map1.getCenter());
    alert('O tipo de mapa atual é: ' + map1.getMapTypeId());
  }
 
  //alterando os valores
  document.getElementById('changeValues').onclick = function() {
    map1.setOptions({
      center: new google.maps.LatLng(40.6891, -74.0445),
      zoom: 17,
      mapTypeId: google.maps.MapTypeId.SATELLITE
    });
  } 
    
};
})();