(function() {

  //definindo as variaveis
  var map, geocoder, infowindow, autocomplete, latLngAtual, positionAtual;
  var markers = [];
  var directionsService = new google.maps.DirectionsService();
  var directionsDisplay = new google.maps.DirectionsRenderer();

  window.onload = function(){

    var defaultBounds = new google.maps.LatLngBounds(new google.maps.LatLng('-33.8902', '151.1759'),new google.maps.LatLng('-33.8474', '151.2631'));

    var options = {  
      zoom: 3,  
      center: new google.maps.LatLng(37.09, -95.71),  
      mapTypeId: google.maps.MapTypeId.ROADMAP  
    };  

    map = new google.maps.Map(document.getElementById('map'), options);

    //autocomplete
    var input = document.getElementById('address');
    var compOpt = {
        bounds: defaultBounds
    };
    autocomplete = new google.maps.places.Autocomplete(input, compOpt);

    autocomplete.addListener('place_changed', onPlaceChanged);

    // verifica se o geo positioning esta disponível
    if (geo_position_js.init()) {

      var settings = {
        enableHighAccuracy: true
      };

      // tenta pegar as coordenadas do usuário
      geo_position_js.getCurrentPosition(setPosition, handleError, settings);

    } else {
      alert('Geo Location não está disponível!');
    }

    //verifica se houve uma mudança no select
    var go = document.getElementById('go');
    go.addEventListener("change", onPlaceChanged);

  };



  function onPlaceChanged() {
    var place = autocomplete.getPlace();
    
    if (place.geometry) {

      map.panTo(place.geometry.location);
      map.setZoom(15);

      //remove os marcadores existentes
      setMapOnAll(null);
      //adicionando o marcador
      var marker = new google.maps.Marker({
        position: place.geometry.location,
        map: map,
        title: 'Clique aqui!'
      });
      markers.push(marker);

      var start = positionAtual;
      var end = document.getElementById('address').value;
      var selectedMode = document.getElementById('go').value;
      var panel = document.getElementById('rota');
      panel.innerHTML = "";

      //calculando a rota
      calcRoute(start,end,map,panel,selectedMode);

    };
  };

  function handleError(error) {
    alert('Ocorreu um erro = ' + error.message);  
  };

  function setPosition(position) {

    // Criando a latitude e longitude com a localização informada
    latLngAtual = new google.maps.LatLng(position.coords.latitude,      
    position.coords.longitude);

    var marker = new google.maps.Marker({
      position: latLngAtual,
      map: map
    });

    markers.push(marker);

    var infoWindow = new google.maps.InfoWindow({
      content: 'Você está aqui!'
    });

    infoWindow.open(map, marker);
    map.setZoom(14);

    //pegando o nome do endereco atual
    var geocoder = new google.maps.Geocoder;
    geocoder.geocode({'latLng': latLngAtual}, function(results, status) {

      if (status == google.maps.GeocoderStatus.OK) {        
        if (results[1]) {
          positionAtual = results[0].formatted_address;
        };

      } else {
        alert("Geocoder Falhou: " + status);
      };

    });

  };

  function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(map);
    }
  };

  //calc route for map
  function calcRoute(start,end,map,panel,selectedMode) {
      
      directionsDisplay.setMap(map);
      directionsDisplay.setPanel(panel);

      var request = {
          origin:start,
          destination:end,
          travelMode: google.maps.TravelMode[selectedMode]
      };
      directionsService.route(request, function(response, status) {
          if (status == google.maps.DirectionsStatus.OK) {
              directionsDisplay.setDirections(response);
          }
      });
  };

})();