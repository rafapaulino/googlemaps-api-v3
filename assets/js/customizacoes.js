(function() {
 window.onload = function() {
   
   
   //noClear (nao limpa o conteudo da div do mapa ao carregar o mapa)  
   var options1 = {
        zoom: 3,
        center: new google.maps.LatLng(17.09, 25.71),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        noClear: true           
     };
   
   var map1 = new google.maps.Map(document.getElementById('map1'), options1);
   
   //background muda o background default durante o carregamento
   var options2 = {
        zoom: 3,
        center: new google.maps.LatLng(27.09, 25.71),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        backgroundColor: '#ff0000'         
     };
   
   var map2 = new google.maps.Map(document.getElementById('map2'), options2);
   
   
   /*
    * draggableCursor 
    * Com essa propriedade, você pode controlar o cursor para usar quando é que paira sobre um objeto arrastável no
        mapa. Você pode definir essa seja fornecendo-lhe o nome de um cursor, como "ponteiro"ou "movimento" ou por
        fornecendo uma URL para uma imagem que você gostaria de usar. Os cursores padrão são os mesmos que você
        pode usar o cursor usando a propriedade CSS. A Tabela 4-1 relaciona os mais úteis. Note que eles podem variar em
        aparência, dependendo de qual sistema operacional e browser você está correndo. Para uma lista completa de cursores
        disponível, vá para http://reference.sitepoint.com/css/cursor.
   */
   var options3 = {
        zoom: 3,
        center: new google.maps.LatLng(17.09, 25.71),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        draggableCursor: 'move'         
     };
   
   var map3 = new google.maps.Map(document.getElementById('map3'), options3);
   
   /* draggingCursor
    * Esta propriedade funciona da mesma maneira como draggableCursor. A única diferença é que controla o cursor
      * ser utilizado enquanto se arrastar um objeto no mapa.
    */
   var options4 = {
        zoom: 3,
        center: new google.maps.LatLng(27.09, 25.71),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        draggingCursor: 'move'      
     };
   
   var map4 = new google.maps.Map(document.getElementById('map4'), options4);
   
 };
})();