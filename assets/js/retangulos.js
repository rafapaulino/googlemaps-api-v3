(function() {

window.onload = function() {

  var options = {
    zoom: 6,
    center: new google.maps.LatLng(36.5, -79.8),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(document.getElementById('map'), options);

  // criando os pontos do primeiro retangulos
  var polyOuter = [
    new google.maps.LatLng(37.303, -81.256),
    new google.maps.LatLng(37.303, -78.333),
    new google.maps.LatLng(35.392, -78.333),
    new google.maps.LatLng(35.392, -81.256)
  ];

  // criando os pontos do segundo retangulo
  var polyInner = [
    new google.maps.LatLng(36.705, -80.459),
    new google.maps.LatLng(36.705, -79),
    new google.maps.LatLng(35.9, -79),
    new google.maps.LatLng(35.9, -80.459)
  ];

  var points = [polyOuter, polyInner];

  // criando os retangulos
  var polygon = new google.maps.Polygon({
    paths: points,
    map: map,
    strokeColor: '#ff0000',
    strokeOpacity: 0.6,
    strokeWeight: 3,
    fillColor: '#FF0000',
    fillOpacity: 0.35
  });

};

})();