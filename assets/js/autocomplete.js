(function() {

  //definindo as variaveis
  var map, geocoder, infowindow, autocomplete;
  var markers = [];

  window.onload = function(){

    var defaultBounds = new google.maps.LatLngBounds(new google.maps.LatLng('-33.8902', '151.1759'),new google.maps.LatLng('-33.8474', '151.2631'));

    var options = {  
      zoom: 3,  
      center: new google.maps.LatLng(37.09, -95.71),  
      mapTypeId: google.maps.MapTypeId.ROADMAP  
    };  

    map = new google.maps.Map(document.getElementById('map'), options);

    var form = document.getElementById('addressForm');

    //autocomplete
    var input = document.getElementById('address');
    var compOpt = {
        bounds: defaultBounds
    };
    autocomplete = new google.maps.places.Autocomplete(input, compOpt);

    autocomplete.addListener('place_changed', onPlaceChanged);

  };



function onPlaceChanged() {
  var place = autocomplete.getPlace();
  
  if (place.geometry) {

    map.panTo(place.geometry.location);
    map.setZoom(15);

    //remove os marcadores existentes
    setMapOnAll(null);
    //adicionando o marcador
    var marker = new google.maps.Marker({
      position: place.geometry.location,
      map: map,
      title: 'Clique aqui!'
    });
    markers.push(marker);

  };
};

function setMapOnAll(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
};

})();