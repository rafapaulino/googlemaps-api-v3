var map, infoWindow;

$(window).load(function(){
 
  var options = {
    zoom: 3,
    center: new google.maps.LatLng(37.09, -95.71),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };

  var map = new google.maps.Map(document.getElementById('map'), options);

  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(40.756054, -73.986951),
    map: map,
    title: 'Clique aqui para ver o balão e mudar o zoom'
  });


  google.maps.event.addListener(marker, 'click', function() {

    var content = document.createElement('div');

    var p = document.createElement('p');
    p.innerHTML = 'Marcador posicionado em Manhattan.';

    var p2 = document.createElement('p');

    // criando o link
    var a = document.createElement('a');
    a.innerHTML = 'Mude o zoom';
    a.href = '#';


    a.onclick = function() {
      map.setCenter(marker.getPosition());
      map.setZoom(15);
      return false;
    };

    p2.appendChild(a);

    content.appendChild(p);
    content.appendChild(p2);

    if (!infoWindow) {
      infoWindow = new google.maps.InfoWindow();
    }

    infoWindow.setContent(content);

    infoWindow.open(map, marker);

  });

});