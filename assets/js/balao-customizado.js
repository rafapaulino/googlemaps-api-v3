(function() {

  var map, infoWindow;

  window.onload = function() {

    var options = {  
      zoom: 3,
      center: new google.maps.LatLng(37.09, -95.71),  
      mapTypeId: google.maps.MapTypeId.ROADMAP  
    };  

    map = new google.maps.Map(document.getElementById('map'), options);

    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(40.756054, -73.986951),
      map: map,
      title: 'Clique aqui'
    });

    google.maps.event.addListener(marker, 'click', function() {

      if (!infoWindow) {
        infoWindow = new google.maps.InfoWindow();
      }
       
      var content = '<div id="info">' +
      '<img src="assets/images/squirrel.jpg" alt="" />' + 
      '<h4>Estudo do Google Maps</h4>' +
      '<p>Texto bacana</p>' +
      '<p><a href="http://www.uol.com.br">UOL</a></p>' +
      '</div>';

      infoWindow.setContent(content);

      infoWindow.open(map, marker);

    });

    google.maps.event.trigger(marker, 'click');

  };

})();