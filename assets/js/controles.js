(function() {
 window.onload = function() {

 //controle normal do mapa
 var options1 = {
     zoom: 3,
     center: new google.maps.LatLng(37.09, -95.71),
     mapTypeId: google.maps.MapTypeId.ROADMAP
 };
 
 //criacao do mapa
 var map = new google.maps.Map(document.getElementById('map1'), options1);
 
 //controle do mapa com a interface de usuario desabilitada
 //doc em http://code.google.com/intl/pt-BR/apis/maps/documentation/javascript/controls.html
 var options2 = {
     zoom: 3,
     center: new google.maps.LatLng(37.09, -95.71),
     mapTypeId: google.maps.MapTypeId.ROADMAP,
	   disableDefaultUI: true
 };
 
 //criacao do mapa
 var map2 = new google.maps.Map(document.getElementById('map2'), options2);
 
 
  /*
   * controle do mapa com a mapTypeControl desabilitada
   * Você o usa para escolher que tipo de mapa
   */
 var options3 = {
     zoom: 3,
     center: new google.maps.LatLng(37.09, -95.71),
     mapTypeId: google.maps.MapTypeId.ROADMAP,
	   mapTypeControl: false
 };
 
 //criacao do mapa
 var map3 = new google.maps.Map(document.getElementById('map3'), options3);
 
   /*
   * controle do mapa com a mapTypeControl exibindo um menu drop-down
   * Você o usa para escolher que tipo de mapa
   */
 var options4 = {
     zoom: 3,
     center: new google.maps.LatLng(37.09, -95.71),
     mapTypeId: google.maps.MapTypeId.ROADMAP,
	   mapTypeControl: true,
     mapTypeControlOptions: {
         style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
     }
 };
 
 //criacao do mapa
 var map4 = new google.maps.Map(document.getElementById('map4'), options4);
 
  /*
   * controle do mapa com a mapTypeControl exibindo a opção de barra horizontal e posicao right
   * Você o usa para escolher que tipo de mapa
   */
 var options5 = {
     zoom: 3,
     center: new google.maps.LatLng(37.09, -95.71),
     mapTypeId: google.maps.MapTypeId.ROADMAP,
	   mapTypeControl: true,
     mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
        position: google.maps.ControlPosition.RIGHT
     }
 };
 
 //criacao do mapa
 var map5 = new google.maps.Map(document.getElementById('map5'), options5);
 
 
   /*
   * controle do mapa com a mapTypeControl exibindo a opção default e com a baara na posicao bottom
   * Você o usa para escolher que tipo de mapa
   * http://code.google.com/intl/pt-BR/apis/maps/documentation/javascript/controls.html
   */
 var options6 = {
     zoom: 3,
     center: new google.maps.LatLng(37.09, -95.71),
     mapTypeId: google.maps.MapTypeId.ROADMAP,
	   mapTypeControl: true,
     mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.DEFAULT,
        position: google.maps.ControlPosition.BOTTOM
     }
 };
 
 //criacao do mapa
 var map6 = new google.maps.Map(document.getElementById('map6'), options6);
 
   /*
   * controle do mapa com a mapTypeControl exibindo a opção default e com a barra na posicao LEFT
   * Você o usa para escolher que tipo de mapa
   * http://code.google.com/intl/pt-BR/apis/maps/documentation/javascript/controls.html
   */
 var options7 = {
     zoom: 3,
     center: new google.maps.LatLng(37.09, -95.71),
     mapTypeId: google.maps.MapTypeId.ROADMAP,
	   mapTypeControl: true,
     mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.DEFAULT,
        position: google.maps.ControlPosition.LEFT
     }
 };
 
 //criacao do mapa
 var map7 = new google.maps.Map(document.getElementById('map7'), options7);
 
 
  /*
   * controle do mapa com a mapTypeControl exibindo a opção default e com a barra na posicao TOP
   * Você o usa para escolher que tipo de mapa
   * http://code.google.com/intl/pt-BR/apis/maps/documentation/javascript/controls.html
   */
 var options8 = {
     zoom: 3,
     center: new google.maps.LatLng(37.09, -95.71),
     mapTypeId: google.maps.MapTypeId.ROADMAP,
	   mapTypeControl: true,
     mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.DEFAULT,
        position: google.maps.ControlPosition.TOP
     }
 };
 
 //criacao do mapa
 var map8 = new google.maps.Map(document.getElementById('map8'), options8);
 
 
   /*
   * controle do mapa com a mapTypeControl exibindo a opção default e com a barra na posicao TOP_LEFT
   * Você o usa para escolher que tipo de mapa
   * http://code.google.com/intl/pt-BR/apis/maps/documentation/javascript/controls.html
   */
 var options9 = {
     zoom: 3,
     center: new google.maps.LatLng(37.09, -95.71),
     mapTypeId: google.maps.MapTypeId.ROADMAP,
	   mapTypeControl: true,
     mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.DEFAULT,
        position: google.maps.ControlPosition.TOP_LEFT
     }
 };
 
 //criacao do mapa
 var map9 = new google.maps.Map(document.getElementById('map9'), options9);
 
 /*
     As opcoes para a barra sao essas
	 • BOTTOM
     • BOTTOM_LEFT
     • BOTTOM_RIGHT
     • LEFT
     • RIGHT
     • TOP
     • TOP_LEFT
     • TOP_RIGHT
 */
 
  //mapa com todas as opcoes de controles de tipo de mapa
  var options10 = {
     zoom: 3,
     center: new google.maps.LatLng(37.09, -95.71),
     mapTypeId: google.maps.MapTypeId.ROADMAP,
	   mapTypeControl: true,
     mapTypeControlOptions: {
        mapTypeIds: [
          google.maps.MapTypeId.ROADMAP,
          google.maps.MapTypeId.SATELLITE,
          google.maps.MapTypeId.HYBRID,
          google.maps.MapTypeId.TERRAIN
        ]
     }
 };
 
 //criacao do mapa
 var map10 = new google.maps.Map(document.getElementById('map10'), options10);
 
   //mapa com 1 das opcoes de controles de tipo de mapa
  var options11 = {
     zoom: 3,
     center: new google.maps.LatLng(37.09, -95.71),
     mapTypeId: google.maps.MapTypeId.ROADMAP,
	   mapTypeControl: true,
     mapTypeControlOptions: {
        mapTypeIds: [
             google.maps.MapTypeId.SATELLITE
        ]
     }
 };
 
 //criacao do mapa
 var map11 = new google.maps.Map(document.getElementById('map11'), options11);
 
 //mapa com 1 das opcoes de controles de tipo de mapa
  var options12 = {
     zoom: 3,
     center: new google.maps.LatLng(37.09, -95.71),
     mapTypeId: google.maps.MapTypeId.HYBRID,
	   mapTypeControl: true,
     mapTypeControlOptions: {
        mapTypeIds: [
             google.maps.MapTypeId.ROADMAP
        ]
     }
 };
 
 //criacao do mapa
 var map12 = new google.maps.Map(document.getElementById('map12'), options12);
	
 };
})();