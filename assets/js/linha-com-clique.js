var map, infoWindow;

$(window).load(function(){

  var options = {
    zoom: 5,
    center: new google.maps.LatLng(36.1834, -117.4960),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(document.getElementById('map'), options);

  // Cria um array mvc
  var route = new google.maps.MVCArray();

  // cria a linha
  var polyline = new google.maps.Polyline({
    path: route,
    strokeColor: "#ff0000",
    strokeOpacity: 0.6,
    strokeWeight: 5
  });

  // adiciona a linha ao mapa
  polyline.setMap(map);

  // adiciona o evento clique
  google.maps.event.addListener(map, 'click', function(e) {

    // pega a referencia do MVCArray
    var path = polyline.getPath();
    // adiciona a posição clicada ao MVCArray
    // adiciona a latitude e longitude
    path.push(e.latLng);

  });

});