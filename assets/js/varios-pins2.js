(function() {

  window.onload = function(){

    var options = {
      zoom: 2,
      center: new google.maps.LatLng(37.09, -95.71),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById('map'), options);

    //espera as coordenadas carregarem
    google.maps.event.addListenerOnce(map, 'bounds_changed', function() {

      var bounds = map.getBounds();

      var southWest = bounds.getSouthWest();
      var northEast = bounds.getNorthEast();

      var latSpan = northEast.lat() - southWest.lat();

      var lngSpan = northEast.lng() - southWest.lng();

      var markers = [];

      // criando o loop
      for (var i = 0; i < 100; i++) {

        var lat = southWest.lat() + latSpan * Math.random();
        var lng = southWest.lng() + lngSpan * Math.random();
        var latlng = new google.maps.LatLng(lat, lng);

        // adicionando o marcador ao mapa
        var marker = new google.maps.Marker({
          position: latlng
        });

        //adiciona o marcador ao array
        markers.push(marker);
      }

      // Criando o MarkerClusterer e adicionando os marcadores nele
      var markerclusterer = new MarkerClusterer(map, markers);

    });

  };

})();