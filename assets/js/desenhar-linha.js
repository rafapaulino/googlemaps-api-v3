var map, infoWindow;

$(window).load(function(){

  var options = {
    zoom: 5,
    center: new google.maps.LatLng(36.1834, -117.4960),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(document.getElementById('map'), options);


  // criando o array que conté as coordenadas da linha
  var route = [
    new google.maps.LatLng(37.7671, -122.4206),
    new google.maps.LatLng(34.0485, -118.2568)
  ];

  // criando a linha
  var polyline = new google.maps.Polyline({
    path: route,
    strokeColor: "#ff0000",
    strokeOpacity: 0.6,
    strokeWeight: 5
  });

  //adicionando a linha ao mapa
  polyline.setMap(map);

});