(function() {
  
  var map, geocoder, infoWindow;

  window.onload = function() {

    var options = {
      zoom: 3,
      center: new google.maps.LatLng(37.09, -95.71),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }; 
    map = new google.maps.Map(document.getElementById('map'), options);

    google.maps.event.addListener(map, 'click', function(e) {

      getAddress(e.latLng);

    });

  };

function getAddress(latLng) {

  if (!geocoder) {
    geocoder = new google.maps.Geocoder();
  };

  var geocoderRequest = {
    latLng: latLng
  };

  geocoder.geocode(geocoderRequest, function(results, status) {

    if (!infoWindow) {
      infoWindow = new google.maps.InfoWindow();
    };

    infoWindow.setPosition(latLng);

    var content = '<h4>Posição: ' + latLng.toUrlValue() + '</h4>';

    if (status == google.maps.GeocoderStatus.OK) {

      // pega as informações retornadas da requisição
      for (var i = 0; i < results.length; i++) {
        if (results[0].formatted_address) {
          content += i + '. ' + results[i].formatted_address + '<br />';          
        }
      }

    } else {
      content += '<p>O endereço não foi encontrado. Status = ' + status + '</p>';
    }

    infoWindow.setContent(content);

    infoWindow.open(map);

  }); 

}

})();