(function() {
 window.onload = function() {
   
   
   //navigationControlOptions
   var options1 = {
           zoom: 3,
           center: new google.maps.LatLng(17.09, -25.71),
           mapTypeId: google.maps.MapTypeId.ROADMAP,
           disableDefaultUI: true
     };
   
   var map1 = new google.maps.Map(document.getElementById('map1'), options1);
   
   //navigationControlOptions tipo small
   var options2 = {
           zoom: 3,
           center: new google.maps.LatLng(-17.09, -45.71),
           mapTypeId: google.maps.MapTypeId.ROADMAP,
           //disableDefaultUI: true,
           navigationControl: true,
           navigationControlOptions: {
              position: google.maps.ControlPosition.TOP_RIGHT,
              style: google.maps.NavigationControlStyle.SMALL
           }
     };
   
   var map2 = new google.maps.Map(document.getElementById('map2'), options2);
   
   
   //navigationControlOptions tipo android
   var options3 = {
           zoom: 3,
           center: new google.maps.LatLng(-27.09, -65.71),
           mapTypeId: google.maps.MapTypeId.ROADMAP,
           navigationControl: true,
           navigationControlOptions: {
                position: google.maps.ControlPosition.TOP_RIGHT,
                style: google.maps.NavigationControlStyle.ANDROID
           }
     };
   
   var map3 = new google.maps.Map(document.getElementById('map3'), options3);
   
   
   //navigationControlOptions tipo zoom pan
   var options4 = {
           zoom: 3,
           center: new google.maps.LatLng(27.09, -65.71),
           mapTypeId: google.maps.MapTypeId.ROADMAP,
           navigationControl: true,
           navigationControlOptions: {
                position: google.maps.ControlPosition.BOTTOM_RIGHT,
                style: google.maps.NavigationControlStyle.ZOOM_PAN
           }
     };
   
   var map4 = new google.maps.Map(document.getElementById('map4'), options4);
   
 
   
   //scaleControl
   // http://code.google.com/intl/pt-BR/apis/maps/documentation/javascript/reference.html#ScaleControlOptions
   var options5 = {
        zoom: 3,
        center: new google.maps.LatLng(27.09, -65.71),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scaleControl: false   
     };
   
   var map5 = new google.maps.Map(document.getElementById('map5'), options5);
   
   /*
    *
    * keyboardShortcuts
    * Esta propriedade habilita ou desabilita a capacidade de usar o teclado para navegar no mapa. 
    * O teclado atalhos que estão disponíveis são as setas de rolagem e + / - para ampliar.
      * Defina esta propriedade como true para os atalhos de teclado para ser ativo ou falso para desativá-los.
    *
      */
    var options6 = {
            zoom: 3,
            center: new google.maps.LatLng(37.09, -95.71),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            keyboardShortcuts: false
      };
    var map6 = new google.maps.Map(document.getElementById('map6'), options6);
    
    
   /*
    *
    * disableDoubleClickZoom
    * Normalmente, quando você clica duas vezes em um mapa, zooms polegadas Para desativar esse comportamento, defina o
      * disableDoubleClickZoom propriedade para true
    *
      */
    var options7 = {
            zoom: 3,
            center: new google.maps.LatLng(37.09, -95.71),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDoubleClickZoom: true
      };
    var map7 = new google.maps.Map(document.getElementById('map7'), options7);
    
    
    // draggable  mover o mapa
    var options8 = {
            zoom: 3,
            center: new google.maps.LatLng(37.09, -95.71),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            draggable: false
      };
    var map8 = new google.maps.Map(document.getElementById('map8'), options8);
    
    // scrollwheel zoom com a rolagem do mouse
    var options9 = {
            zoom: 3,
            center: new google.maps.LatLng(37.09, -95.71),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false
      };
    var map9 = new google.maps.Map(document.getElementById('map9'), options9);
    
    // streetViewControl controle do street view
    var options10 = {
            zoom: 3,
            center: new google.maps.LatLng(37.09, -95.71),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            streetViewControl: false
      };
    var map10 = new google.maps.Map(document.getElementById('map10'), options10);
    
 };
})();