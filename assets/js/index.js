/*
 * Documentação em: http://code.google.com/intl/pt-BR/apis/maps/documentation/javascript/tutorial.html
 */
(function() {
  window.onload = function() {
	 
	 //latitude - longitude
	 var latlng = new google.maps.LatLng(37.09, -95.71);

	 //cria a referencia para o primeiro mapa
	 var mapDiv1 = document.getElementById('map1');
	//cria as opcoes para o mapa
	 var options1 = {
	  //Define o centro do mapa com coordenadas. As coordenadas devem ser do tipo google.maps.LatLng
	  center: latlng, 
	  /* 
	   * Define o primeiro nível de zoom no mapa. Deve ser um número entre 1 e 23,
       * onde 1 é ampliado todo o caminho e 23 é ampliado até o acesso (mais profundo nível de zoom realmente 
	   * pode variar dependendo dos dados de mapas disponíveis.)
	   */
	  zoom: 4, 
	  //tipo de mapa rua, fotos, terreno, hibrido (fotos e ruas)
      mapTypeId: google.maps.MapTypeId.ROADMAP  //tipo de mapa para o primeiro mapa (mapa normal, tipo guia de ruas)
    };
	//cria o mapa
	var map1 = new google.maps.Map(mapDiv1, options1);
	
	
	 //cria a referencia para o segundo mapa
	 var mapDiv2 = document.getElementById('map2');
	//cria as opcoes para o mapa
	 var options2 = {
	  center: latlng, 
	  zoom: 4, 
      mapTypeId: google.maps.MapTypeId.SATELLITE  //tipo de mapa para o segundo mapa (mostra a foto do lugar, tipo satelite)
    };
	//cria o mapa
	var map2 = new google.maps.Map(mapDiv2, options2);
	
	//cria a referencia para o terceiro mapa
	 var mapDiv3 = document.getElementById('map3');
	//cria as opcoes para o mapa
	 var options3 = {
	  center: latlng, 
	  zoom: 4, 
      mapTypeId: google.maps.MapTypeId.HYBRID  //tipo de mapa para o terceiro mapa (mostra a foto do lugar, tipo satelite e as ruas)
    };
	//cria o mapa
	var map3 = new google.maps.Map(mapDiv3, options3);
	
	
	//cria a referencia para o quarto mapa
	 var mapDiv4 = document.getElementById('map4');
	//cria as opcoes para o mapa
	 var options4 = {
	  center: latlng, 
	  zoom: 4, 
      mapTypeId: google.maps.MapTypeId.TERRAIN  //tipo de mapa para o quarto mapa (mostra o terreno)
    };
	//cria o mapa
	var map4 = new google.maps.Map(mapDiv4, options4);
	
	  
	  
  }
})();