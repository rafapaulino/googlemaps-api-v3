(function() {

  // carregando a Google Maps API
  google.load('maps', 3, {
    'other_params': 'sensor=false&language=pt_BR'
  });

  window.onload = function() {

    //pega a posição atual
    if (google.loader.ClientLocation.latitude && google.loader.ClientLocation.longitude) {

      //define a posição
      var latLng = new google.maps.LatLng(google.loader.ClientLocation.latitude, 
               google.loader.ClientLocation.longitude);

      // Cria o conteúdo do balão
      var location = 'Você está em: '
        location += google.loader.ClientLocation.address.city + ', '; 
        location += google.loader.ClientLocation.address.country;

    } else {
      
      var latLng = new google.maps.LatLng(0, 0);
      var location = 'Sua localização não foi encontrada!';
    }

    var options = {
      zoom: 2,
      center: latLng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }; 

    map = new google.maps.Map(document.getElementById('map'), options);

    var marker = new google.maps.Marker({
      position: latLng,
      map: map
    });

    var infoWindow = new google.maps.InfoWindow({
      content: location
    });

    infoWindow.open(map, marker);

  };

})();
