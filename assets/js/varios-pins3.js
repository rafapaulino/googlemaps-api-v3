(function() {

  window.onload = function() {

    var options = {
      zoom: 4,
      center: new google.maps.LatLng(37.99, -93.77),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById('map'), options);

    // Criando o objeto do MarkerManager
    var mgr = new MarkerManager(map);

    // Criando um array para guardar os marcadores
    var markers = [];

    // Setando as coordenadas de onde eles serão criados
    var southWest = new google.maps.LatLng(24, -126);
    var northEast = new google.maps.LatLng(50, -60);
    var lngSpan = northEast.lng() - southWest.lng();
    var latSpan = northEast.lat() - southWest.lat();

    // Criando os marcadores em posições randômicas
    for (var i = 0; i < 100; i++) {

      var lat = southWest.lat() + latSpan * Math.random();
      var lng = southWest.lng() + lngSpan * Math.random();
      var latlng = new google.maps.LatLng(lat, lng);

      var marker = new google.maps.Marker({
        position: latlng
      });

      markers.push(marker);
    }

    // Coloca os marcadores após o mapa ser carregado
    google.maps.event.addListener(mgr, 'loaded', function() {

      mgr.addMarkers(markers, 1);
      mgr.refresh();

    });
  };

})();