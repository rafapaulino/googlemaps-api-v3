(function() {

  window.onload = function(){

    var options = {
      zoom: 2,
      center: new google.maps.LatLng(37.09, -95.71),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    
    var map = new google.maps.Map(document.getElementById('map'), options);

    //espera as coordenadas carregarem
    google.maps.event.addListenerOnce(map, 'bounds_changed', function() {

      //pega o tamanho do mapa
      var bounds = map.getBounds();

      //pega os cantos do mapa
      var southWest = bounds.getSouthWest();
      var northEast = bounds.getNorthEast();

      //calcula a distância entre o topo e o rodapé do mapa
      var latSpan = northEast.lat() - southWest.lat();

      //calcula a distância entre a esquerda e direita do mapa
      var lngSpan = northEast.lng() - southWest.lng();

      //cria o loop
      for (var i = 0; i < 100; i++) {

        //cria uma posição randômica
        var lat = southWest.lat() + latSpan * Math.random();
        var lng = southWest.lng() + lngSpan * Math.random();
        var latlng = new google.maps.LatLng(lat, lng);

        //adiciona o marcador ao mapa
        new google.maps.Marker({
          position: latlng, 
          map: map
        });

      }

    });

  };

})();