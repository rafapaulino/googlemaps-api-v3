(function() {

var map, infoWindow;

  window.onload = function() {

    var options = {  
      zoom: 3,
      center: new google.maps.LatLng(37.09, -95.71),  
      mapTypeId: google.maps.MapTypeId.ROADMAP  
    };  

    map = new google.maps.Map(document.getElementById('map'), options);

    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(40.756054, -73.986951),
      map: map,
      title: 'Clique aqui'
    });


    google.maps.event.addListener(marker, 'click', function() {

      // criando um mapa com os detalhes do local
      var detailDiv = document.createElement('div');
      detailDiv.style.width = '200px';
      detailDiv.style.height = '200px';
      document.getElementById('map').appendChild(detailDiv);

      // mapa com detalhes
      var overviewOpts = {
        zoom: 14,
        center: marker.getPosition(),
        mapTypeId: map.getMapTypeId(),
        disableDefaultUI: true
      };

      var detailMap = new google.maps.Map(detailDiv, overviewOpts);

      // criando um marcador no mapa dos detalhes
      var detailMarker = new google.maps.Marker({
        position: marker.getPosition(),
        map: detailMap,
        clickable: false
      });


      if (!infoWindow) {
        infoWindow = new google.maps.InfoWindow();
      }

      infoWindow.setContent(detailDiv);

      infoWindow.open(map, marker);

    });

  };
})();