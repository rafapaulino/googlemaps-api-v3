(function() {

  var map;

  window.onload = function() {

    var options = {
      zoom: 1,
      center: new google.maps.LatLng(31.35, 3.51),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }; 

    map = new google.maps.Map(document.getElementById('map'), options);

    // verifica se o geo positioning esta disponível
    if (geo_position_js.init()) {

      var settings = {
        enableHighAccuracy: true
      };

      // tenta pegar as coordenadas do usuário
      geo_position_js.getCurrentPosition(setPosition, handleError, settings);

    } else {
      alert('Geo Location não está disponível!');
    }

  };

  function handleError(error) {
    alert('Ocorreu um erro = ' + error.message);  
  };

  function setPosition(position) {

    // Criando a latitude e longitude com a localização informada
    var latLng = new google.maps.LatLng(position.coords.latitude,      
    position.coords.longitude);

    var marker = new google.maps.Marker({
      position: latLng,
      map: map
    });

    var infoWindow = new google.maps.InfoWindow({
      content: 'Você está aqui!'
    });

    infoWindow.open(map, marker);
    map.setZoom(6);

  };

})();