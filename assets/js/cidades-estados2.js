(function() {

  window.onload = function(){

    var options = {
      zoom: 3,
      center: new google.maps.LatLng(37.99, -93.77),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById('map'), options);

    var mgr = new MarkerManager(map);

    //Colorado
    var colorado = new google.maps.Marker({
      position: new google.maps.LatLng(39.4568, -105.8532),
      icon: 'assets/images/cluster.png'
    });

    google.maps.event.addListener(colorado, 'click', function() {

      map.setZoom(7);
      map.setCenter(colorado.getPosition());

    });

    //Utah
    var utah = new google.maps.Marker({
      position: new google.maps.LatLng(40.0059, -111.9176),
      icon: 'assets/images/cluster.png'
    });

    google.maps.event.addListener(utah, 'click', function() {
      map.setZoom(7);
      map.setCenter(utah.getPosition());
    });

    var states = [colorado, utah];

    var cities = [
      // Colorado Springs
      new google.maps.Marker({position: new google.maps.LatLng(38.8338, -104.8213)}), 
      // Denver
      new google.maps.Marker({position: new google.maps.LatLng(39.7391, -104.9847)}),
      // Glenwood Springs
      new google.maps.Marker({position: new google.maps.LatLng(39.5505, -107.3247)}), 
      // Salt Lake City
      new google.maps.Marker({position: new google.maps.LatLng(40.7607, -111.8910)}), 
      // Fillmore
      new google.maps.Marker({position: new google.maps.LatLng(38.9688, -112.3235)}), 
      // Spanish Fork
      new google.maps.Marker({position: new google.maps.LatLng(40.1149, -111.6549)})
    ];

    google.maps.event.addListener(mgr, 'loaded', function() {

      mgr.addMarkers(states, 1, 5);

      mgr.addMarkers(cities, 6);

      mgr.refresh();

    });

  };

})();