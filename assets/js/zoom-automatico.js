(function() {
window.onload = function() {
  //este codigo deixa apenas 1 janela aberta por vez, diferente do anterior
  //e faz o zoom do mapa se ajustar ao total de pins existentes nele

  //criando as propriedades do mapa 
  var options1 = {
    zoom: 3,
    center: new google.maps.LatLng(37.09, -95.71),
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    noClear: true ,
    backgroundColor: '#FFFFFF',
    draggableCursor: 'move',
    draggingCursor: 'move',
    scrollwheel: false
  };

  var map1 = new google.maps.Map(document.getElementById('map'), options1);

  //cria o objeto que serve como delimitador da area de visao do mapa
  var bounds = new google.maps.LatLngBounds();

  //cria um array para os lugares 
  var places = [];

  // Adding a LatLng object for each city
  places.push(new google.maps.LatLng(40.756, -73.986));
  places.push(new google.maps.LatLng(37.775, -122.419));
  places.push(new google.maps.LatLng(47.620, -122.347));
  places.push(new google.maps.LatLng(-22.933, -43.184));

  //cria a variavel para infowindow
  var infowindow;


  //faz o loop pelos elementos 
  for (var i = 0; i < places.length; i++) { 

    //adicionando o marcador
    var marker = new google.maps.Marker({
      position: places[i], 
      map: map1,
      title: 'Aqui caiu o avião número ' + i,
      icon: 'assets/images/aviao.png'
    });

    // Wrapping the event listener inside an anonymous function 
    // that we immediately invoke and passes the variable i to.
    (function(i, marker) {
      // Creating the event listener. It now has access to the values of
      // i and marker as they were during its creation
      google.maps.event.addListener(marker, 'click', function() {

        if (!infowindow) {
          infowindow = new google.maps.InfoWindow();
        }

        //insere o conteudo dentro da janela
        infowindow.setContent('Este é o avião número' + i);

        infowindow.open(map1, marker);
      });

    })(i, marker);

    //extende o objeto aos lugares
    bounds.extend(places[i]);

  }

  //ajusta o zoom do mapa de acordo com a area de pins
  map1.fitBounds(bounds);
};
})();