<?php include 'header.html'; ?>

  <div class="row">
    <div class="col-md-6">
      <div id="map1" class="mapsControles"></div>
      <p><strong>Não limpa o conteudo da div do mapa ao carregar o mapa</strong></p>
    </div>
    <div class="col-md-6">
      <div id="map2" class="mapsControles"></div>
      <p><strong>Muda o background default durante o carregamento</strong></p>
    </div>
  </div>

  <div class="row">
    <div class="col-md-6">
      <div id="map3" class="mapsControles"></div>
      <p><strong>Muda o cursor ao passar o mouse sobre o mapa</strong></p>
    </div>
    <div class="col-md-6">
      <div id="map4" class="mapsControles"></div>
      <p><strong>Muda o cursor ao arrastar o mapa</strong></p>
    </div>
  </div>

<script type="text/javascript" src="assets/js/customizacoes.js"></script>
<?php include 'footer.html'; ?>