<?php include 'header.html'; ?>

  <div class="row">
    <div class="col-md-6">
      <div id="map1" class="mapsIndex"></div>
      <p><strong>Mapa Normal</strong></p>
    </div>
    <div class="col-md-6">
      <div id="map2" class="mapsIndex"></div>
      <p><strong>Mapa Satélite</strong></p>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div id="map3" class="mapsIndex"></div>
      <p><strong>Mapa Híbrido</strong></p>
    </div>
    <div class="col-md-6">
      <div id="map4" class="mapsIndex"></div>
      <p><strong>Mapa Terreno</strong></p>
    </div>
  </div>

<script type="text/javascript" src="assets/js/index.js"></script>
<?php include 'footer.html'; ?>