<?php include 'header.html'; ?>

  <div class="row">
    <div class="col-md-12">
		<p>Pega a latitude e longitude do endereço.</p>
		<form id="addressForm" action="" class="form-inline">
			<div class="form-group">
				<label for="address">Endereço:</label>
				<input type="text" name="address" id="address" class="form-control">					
			</div>
			<input type="submit" class="btn btn-primary" id="addressButton" value="Pegar valores">
		</form>
		<div id="map" class="valores"></div>
    </div>
  </div>

<script type="text/javascript" src="assets/js/latlng.js"></script>
<?php include 'footer.html'; ?>