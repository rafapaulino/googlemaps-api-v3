<?php include 'header.html'; ?>

  <div class="row">
    <div class="col-md-12">
		<p>Adiciona marcadores para cidades e estados, clique no marcador do estado para ver as cidades.</p>
		<div id="map" class="valores"></div>
    </div>
  </div>

<script type="text/javascript" src="assets/js/lib/markermanager_packed.js"></script>
<script type="text/javascript" src="assets/js/cidades-estados2.js"></script>
<?php include 'footer.html'; ?>